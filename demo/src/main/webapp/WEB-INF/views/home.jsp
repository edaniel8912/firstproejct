<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<title>Home</title>
</head>
<script>

$( function(){
	
	$('#testJs').off().on('click', function () {
		console.log('testJs');
		var sendData = {
                name: "js_tttt",
                age: '111'
        }
		// js Ajax
		var xhr = new XMLHttpRequest();
	    xhr.onreadystatechange = function() {
	        if (xhr.readyState === xhr.DONE) {
	            if (xhr.status === 200 || xhr.status === 201) {
	            	var data = JSON.parse(xhr.responseText);
	                console.log( data );
	                var result = data.usr;
	                var tmp = $('#tempP').clone();
					tmp.removeAttr('id');
					tmp.text( result.name );
					$('#addDiv').append( tmp );
	    	       } else {
	               console.error(xhr.responseText);
	            }
	        }
	    };
	    xhr.open('POST', '/test.ajax');
	    xhr.setRequestHeader('Content-Type', 'application/json');
	    xhr.send( JSON.stringify(sendData) );
		
	});
	
	$('#testJquery').off().on('click', function () {
		console.log('testJquery');
		var sendData = {
                name: "jQuery_tttt",
                age: '222'
        }
		// jQuery Ajax
		$.ajax({
			type: "POST",
			url: '/test.ajax', 
			data: JSON.stringify(sendData),
			dataType: "json",
			contentType:"application/json;charset=UTF-8",
			async: true,
			success : function(data, status, xhr) {
				console.log(data);
				var result = data.usr;
				var tmp = $('#tempP').clone();
				tmp.removeAttr('id');
				tmp.text( result.name );
				$('#addDiv').append( tmp );
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
		});
		
	});
	
});

</script>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
<P>  Gitlab Jenkins Build Success </P>
<button id="testJquery">test jQuery</button>
<button id="testJs">test js</button>
<p id="tempP"></p>
<div id="addDiv">
</div>
</body>
</html>
